Trigger Happy: Shockingly Simple Data Editing in the Enterprise
===============================================================

## Preamble

We need to restore some data. Drop into your shell and execute the following:

```sh
docker exec advanced-postgis-workshop_pgsql_1 pg_restore -d workshop -h localhost -p 44449 -U workshop /files/kgn21.backup
```
We will also be using QGIS for this exercise, so if you havent opened it yet, you can do so now.

## The Scenario: Triggers for Enforcing Business Logic

You are the manager of a GIS Department at a small power distribution company. Recently, an Outage Management System was purchased and relies heavily on an accurate and up to date GIS. The OMS pulls data from the GIS every night. There are several requirements that the OMS stipulates. We will be focussing on two, these are:

 1. Switches and fuses, which are modeled as points, must not be in a line. They must be at the end, or beginning of a linestring.
 2. Service points must connect to the nearest secondary line via a `service_wire` table.

You do not want to have your GIS Technicians breaking a linestring and then inserting a point or adding vertices before drawing a line. Instead, you are to create triggers that accomplish both fuctions.

### Background

The data for the Electric distribution network is stored in the `kgn21` schema. The schema contains 14 tables. For part 1, we are interested in 4 of them. They are:
 - `kgn21.interrupting_devices`
 - `kgn21.isolating_devices`
 - `kgn21.primary_lines_ag`
 - `kgn21.primary_lines_ug`

For part 2 the following are of interest:
 - `kgn21.service_location`
 - `kgn21.secondary_line_ag`
 - `kgn21.secondary_line_ug`


## The Solution for part 1

### What seems to be the problem here?

Let us first identify what we want and what we do not want. Using QGIS, look for `device_id`s 119720 AND 118438 in the `kgn21.interrupting_devices` table. `device_id` 119720 does not meet the OMS requirements while `device_id` 118438 is fine because it is between two line segments (you can verify this in QGIS).

The following query shows a list of topological relationships along with the [Dimensionally Extended 9-Intersection Model matrix](https://postgis.net/docs/manual-3.2/using_postgis_query.html#DE-9IM) for both devices.

```SQL
SELECT
 st_intersects(id.g, plag.g), st_touches(id.g, plag.g), st_contains(id.g, plag.g), st_containsproperly(id.g, plag.g),
 st_crosses(id.g, plag.g), st_covers(id.g, plag.g), st_coveredby(id.g, plag.g), st_overlaps(id.g, plag.g),
 st_relate(id.g, plag.g), id.device_id point_id, plag.device_id line_id
FROM kgn21.interrupting_devices id
JOIN kgn21.primary_lines_ag plag ON st_intersects(id.g, plag.g)
WHERE id.device_id IN (119720, 118438)
```
Based on the results of the query, we are looking for (in order of simplicity):
 - points that `st_intersects` a line, but do not share an start or endpoint.
 - points that `st_intersects` a line but does not `st_touches` the same line.
 - We are looking for points that `st_intersects` a line and for which the following DE-9IM matrix is true: `0FFFFF102`.

Note that all of the above are the same thing.

So, either of the following queries will work:

```SQL
SELECT count(*) FROM kgn21.interrupting_devices id
JOIN kgn21.primary_lines_ag plag ON st_intersects(id.g, plag.g) AND NOT
  (st_equals(st_startpoint(plag.g), id.g) OR st_equals(st_endpoint(plag.g), id.g))
```

```SQL
SELECT count(*) FROM kgn21.interrupting_devices id
JOIN kgn21.primary_lines_ag plag ON st_intersects(id.g, plag.g) AND NOT st_touches(id.g, plag.g)
```

```SQL
SELECT count(*) FROM kgn21.interrupting_devices id
JOIN kgn21.primary_lines_ag plag ON st_relate(id.g, plag.g, '0FFFFF102') AND plag.g && id.g
```

### Pulling the trigger

As was stated in the scenario we need to implement a trigger. This consists of two things
 1. Writing a trigger function.
 2. Defining a trigger on a table.

A trigger function is just like any other function in PostgreSQL except that it returns a type trigger. Lets so some pseudo-code first to get an idea of what will happen when a point is added.

``` pseudo-code
1. Get point coordinates
2. Determine if point st_intersects primary_lines_ag OR primary_lines_ug
3. If point st_intersects st_startpoint or st_endpoint of line then exit. we're good.
4. split line (How?) into two parts.
5. Update existing line with part 1 of line.
6. Insert into line table part 2 of line.
7. Update point bsed on line elevation
```

Lets talk about #4. There are probably a number of different was we could tackle this. We could look at PostGIS' [linear referenceing](https://postgis.net/docs/manual-3.2/reference.html#Linear_Referencing) functions and use that in conjuction with functions to add a point to a line, however there is an `st_split` function which does exactly what we need.

So, lets create the trigger function.

```SQL
CREATE OR REPLACE FUNCTION kgn21.isolator_interruptor_primary_line_splitter()
RETURNS trigger as
$trigger$
DECLARE
 split_g geometry;
 pl_rec record;

BEGIN
	/* first check to see if the pointintersects a primary_lines feature */
 SELECT * FROM kgn21.primary_lines_ag WHERE st_intersects(g, NEW.g) INTO pl_rec;
 IF pl_rec.device_id IS NOT NULL THEN -- if it does, drop into this if block
  split_g = st_split(pl_rec.g, NEW.g); -- st_split returns a geometrycollection.
  /* st_geometryn selects the nth geometry from a geometrycollection or a multi geometry */
  UPDATE kgn21.primary_lines_ag SET g = st_geometryn(split_g,1) WHERE device_id = pl_rec.device_id;
  INSERT INTO kgn21.primary_lines_ag (feeder_name, phase_data, g) VALUES (pl_rec.feeder_name, pl_rec.phase_data, st_geometryn(split_g, 2));
  NEW.g = st_force3d(st_force2d(NEW.g), 10); -- st_force3d converts a 2d geometry into a 3d geometry
 ELSE -- if we drop into this blockthe previous SELECT query returned null
  SELECT * FROM kgn21.primary_lines_ug WHERE st_intersects(g, NEW.g) INTO pl_rec;
  split_g = st_split(pl_rec.g, NEW.g);
  UPDATE kgn21.primary_lines_ug SET g = st_geometryn(split_g,1) WHERE device_id = pl_rec.device_id;
  INSERT INTO kgn21.primary_lines_ug (feeder_name, phase_data, g) VALUES (pl_rec.feeder_name, pl_rec.phase_data, st_geometryn(split_g, 2));
  NEW.g = st_force3d(st_force2d(NEW.g), -5);
 END IF;
 NEW.phase_data = pl_rec.phase_data;

 RETURN NEW;

END;

$trigger$ LANGUAGE plpgsql;
```

Lets now add the trigger to the tables:

```SQL
CREATE TRIGGER "1: Handle line splitting for isolating and interrupting devices"
 BEFORE INSERT OR UPDATE OF g
 ON kgn21.isolating_devices
 FOR EACH ROW EXECUTE FUNCTION kgn21.isolator_interruptor_primary_line_splitter();
```
Lets give it a whirl. Using QGIS try and add an new point to the `kgn21.interrupting_devices` table.


## The Solution for part 2

Well, lets see if you can hash this one out on your own. I'll give you a hint: Postgis' Linear referencing functions will be of use.

