Necessity - the Mother of Invention: Faking it 'till you Make it
================================================================

## Preamble

Occasionally, you may want to visualize data that you don't exactly have. For example you may have the number of trees in a given farm plot, but it would be nice to actually show the number of trees with nice little points on a map. Conversely, you may have data you want to show, but for various reasons, you can;t show the exact location. These are some examples.

## Lo Scenario: La popolazione d'Italia

Your new boss has the immense need to see the population of Tuscany, Italy on a map. One point per human being. He hasn't told you why. But today is one of those day you ignore the nonsense and provide results.



## The Data

First, lets get this data. We are going to use two new sets of data. One is from (gadm.org)[https://gadm.org/] and the other is from the geonames project. Lets get the gadm data first.


```SQL
CREATE SCHEMA gadm;  -- can't start without a new schema
CREATE SERVER gadm_ita FOREIGN DATA WRAPPER ogr_fdw
  OPTIONS ( -- we're using the nifty virtual file system here. No need for pre downloads
   datasource '/vsicurl/https://geodata.ucdavis.edu/gadm/gadm4.1/gpkg/gadm41_ITA.gpkg',
   format 'GPKG');
IMPORT FOREIGN SCHEMA ogr_all  FROM SERVER gadm_ita INTO gadm
  OPTIONS (launder_table_names 'false', launder_column_names 'false');
```
The table that we are interested in is `ADM_ADM_3`. While foreign tables are really cool, we ant the entire thing locally for performance reasons.

```SQL
CREATE TABLE gadm.italy AS
SELECT * FROM gadm."ADM_ADM_3";
ALTER TABLE gadm.italy ALTER COLUMN geom TYPE Geometry(Multipolygon, 7794) USING st_transform(geom, 7794);
```

Now for the geonames data. This is not is a spatial format. The data we want is in a CSV file. We can use postgresqls COPY command to import the data. Let us first define our table:

```SQL
CREATE SCHEMA geonames;
CREATE TABLE geonames.italy (
 geonameid integer primary key,
 name text,
 asciiname text,
 alternatenames text,
 latitude float8,
 longitude float8,
 featureclass text,
 featurecode text,
 countrycode text,
 countrycode2 text,
 admin1 text,
 admin2 text,
 admin3 text,
 admin4 text,
 population bigint,
 elevation integer,
 dem integer,
 timezone text,
 modified date
);
```
Now lets pull the data in directly from the web:

```SQL
copy geonames.italy FROM PROGRAM
'curl https://download.geonames.org/export/dump/IT.zip --output pp.zip ; unzip -qq pp.zip IT.txt ; cat IT.txt'
  WITH (FORMAT csv, DELIMITER E'\t', HEADER false, ENCODING utf8);
```
Lets add a geometry column and update it with the lat/lon info:

```SQL
ALTER TABLE geonames.italy ADD COLUMN geo geometry(Point, 7794);
UPDATE geonames.italy set geo = st_transform(st_setsrid(st_makepoint(longitude, latitude), 4326), 7794);
```

And lastly lets index the data:

```SQL
CREATE INDEX ON geonames.italy USING GIST (geo);
CREATE INDEX ON gadm.italy USING GIST (geom);
```

Great, our data is all ready.

## Making people

The population data is stored in the geonames table. The following query joins the geonames table and the ital table. There is alot of extra checks because the geonames table contains several variations of the names.

```SQL
WITH _ AS (
  SELECT "NAME_3", name, population, featurecode, geom  FROM gadm.italy
  left JOIN geonames.italy on ST_intersects(geom,geo) AND replace(lower("NAME_3"),' ','') = replace(lower(name),' ','') AND (featurecode = 'ADM3' OR featurecode = 'PPLA3')
  WHERE name   IS not  NULL AND "NAME_1" = 'Toscana'
  ORDER BY 1
  )
SELECT name,  avg(population)::int, geom FROM _
group by name, geom;
```

We can use the st_generatepoints function to generate random points in each polygon. By default, it returns one multipoint per polygon. Lets see what that looks like:

```SQL
CREATE SCHEMA italy;
CREATE TABLE italy.population_mp AS
WITH _ AS (
  SELECT "NAME_3", name, population, featurecode, geom  FROM gadm.italy
  left JOIN geonames.italy on ST_intersects(geom,geo) AND replace(lower("NAME_3"),' ','') = replace(lower(name),' ','') AND (featurecode = 'ADM3' OR featurecode = 'PPLA3')
  WHERE name   IS not  NULL AND "NAME_1" = 'Toscana'
  ORDER BY 1
  ),
 __ AS (
  SELECT name,  avg(population)::int, geom FROM _
  group by name, geom)
  SELECT name, avg, st_generatepoints(geom, avg) geom FROM __;
```

Lets see what it is like (and how long it takes) to break out each point as its own entity:

```SQL
CREATE TABLE italy.population_p AS
  WITH _ AS (
			SELECT  (st_dumppoints(geom)).geom FROM italy.population_mp)
  SELECT row_number() OVER () rid, geom FROM _;
```
Let us compare the tables:

```SQL
SELECT count(*) FROM italy.population_mp;
```

```SQL
SELECT count(*) FROM italy.population_p;
```

```SQL
CREATE INDEX ON ital.population_p USING GIST (geom);
```

## Summarising Data

On the flip side, you may have lots of individual points, but you want to generalize over an are. Usually, there may be a commune or parish, but occasionally you want something abstract. Like hexagons. Because, hexagons are th bestagons.

Lets create a 1km hex grid over the Tuscany region:

```SQL
CREATE TABLE italy.hex_1k AS
WITH gridbox AS (
select st_envelope(st_union(geom)) gridbox FROM gadm.italy where "NAME_1" = 'Toscana'
)
SELECT * FROM st_hexagongrid(1000, (select gridbox from gridbox));
```
Add the resulting table to QGIS. What do you notice?

Lets remove the extraneous hexagons:

```SQL
DELETE FROM italy.hex_1k WHERE NOT
	st_intersects(geom,
	(SELECT st_union(geom) from gadm.italy WHERE "NAME_1" = 'Toscana'));
```

And we create an index

```SQL
CREATE INDEX ON italy.hex_1k USING GIST (geom);
```

## Bonus Question

Can you create a simple map in QGIS showing hexagon population by colour?

