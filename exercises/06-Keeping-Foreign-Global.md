Keeping Foreign Global: Travelling Vicariously Without Moving
=============================================================

## The Scenario: How Can we Get There?

Your friends are very excited that you are going to FOSS4G 2022 in Firenze, Italy. They now want to know which countries they would travel over if they went to Firenze from their home town. But they aren't in posession of any desktop tools or postgis (They just aren't techie like that, despite your best efforts to encourage them). So, you decide to create a website where they can click on a map, and the path to Firenze along with the countries they fly over will be displayed.

## The Tools

So, to start, you did the bulk of the work in Exercise 2. What is now needed is:
 1. a way to get data from the database out to the web, and
 2. an easily repeatable way of encapsulating the process you did in Exercise 2 with new coordinates.

Lets start by trying to solve #2 first. Generally if you want to make something easily repeatable you would use a function. Postgresql has several procedural languages which can be used to create functions. We're gonna stick to plpgsql.

We create a schema to store our web stuff:

```SQL
/* This is the weirdest bug...and was very ard to track down */
drop SERVER gadm_ita CASCADE ;
```


```SQL
CREATE SCHEMA api;
```
Open `http://localhost:44440/keeping-foreign-global.html` in a browser and then open developer tools.

Lets start simply by trying to get the path. The function takes in the lat & lon from the point that was clicked in the browser window and returns geojson.

```SQL
CREATE OR REPLACE FUNCTION api.to_firenze(IN lng float, IN lat float, OUT info json)
AS
$$
 BEGIN
  -- We've seen both the st_makeline & st_makepoint functions.
  -- st_asgeojson does what you expect, converts a geometry/geography into a geoJSON fragment.
  info = '{"type": "Feature","properties": {}, "geometry":' ||st_asgeojson(st_makeline(st_makepoint(lng, lat),st_makepoint(11.25, 43.75)))||'}';
 END;
$$
LANGUAGE plpgsql;
NOTIFY pgrst, 'reload schema';
```

```SQL
CREATE FUNCTION api.to_firenze(IN lng float, IN lat float, OUT info json)
AS
$$
 BEGIN
  -- We've seen both the st_makeline & st_makepoint functions.
  -- st_asgeojson does what you expect, converts a geometry/geography into a geoJSON string.
  info = st_asgeojson(st_makeline(st_makepoint(lon, lat),st_makepoint(11.25, 43.75)));
 END;
$$
LANGUAGE plpgsql
```
A couple things to note, we do not need an explicit `RETURN` as we are using IN & OUT attributes.
So, we have the path working, lets try and get the countries as well.

```SQL
CREATE OR REPLACE FUNCTION api.to_firenze(IN lng float, IN lat float, OUT info json)
AS
$$
 DECLARE
  path geometry(Linestring, 4326);
  pays geometry;

 BEGIN
  path = st_makeline(st_makepoint(lng, lat),st_makepoint(11.25, 43.75));

  SELECT st_union(geo::geometry) FROM natural_earth.countries where
  st_intersects(path, geo) INTO pays;

  info = st_asgeojson(st_collect(path, pays));

END;
$$
LANGUAGE plpgsql;
NOTIFY pgrst, 'reload schema';
```

Hoooray!!!!!! how does this compare to the data from the table `recap.countries_i_flew_over_to_get_here` that you created in Exercise #2? Well, the function is using the geometry type and not the geography type....so that could cause an issue. Let's fix it.

```SQL
CREATE OR REPLACE FUNCTION api.to_firenze(IN lng float, IN lat float, OUT info json)
AS
$$
 DECLARE
  path geography(Linestring, 4326);
  pays geography;

 BEGIN
  path = st_makeline(st_makepoint(lng, lat),st_makepoint(11.25, 43.75));

  SELECT st_union(geo::geometry) FROM natural_earth.countries where
  st_intersects(path, geo) INTO pays;

  info = st_asgeojson(st_collect(path::geometry, pays::geometry));

END;
$$
LANGUAGE plpgsql;
NOTIFY pgrst, 'reload schema';
```

Depending on where you clicked, you may notice that the path does not intersect one or more countries. This can be easily fixed using st_segmentize.

```SQL
CREATE OR REPLACE FUNCTION api.to_firenze(IN lng float, IN lat float, OUT info json)
AS
$$
 DECLARE
  path geography(Linestring, 4326);
  pays geography;

 BEGIN
  path = st_segmentize(st_makeline(st_makepoint(lng, lat),st_makepoint(11.25, 43.75))::geography, 50000);

  SELECT st_union(geo::geometry) FROM natural_earth.countries where
  st_intersects(path, geo) INTO pays;

  info = st_asgeojson(st_collect(path::geometry, pays::geometry));

END;
$$
LANGUAGE plpgsql;
NOTIFY pgrst, 'reload schema';
```

Ok, your friends now have a nifty website to see which countries they will fly over on their way to Firenze. It just needs a bit of love in the style department.

