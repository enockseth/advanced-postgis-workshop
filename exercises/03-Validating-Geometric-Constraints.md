Geometries: A brief exploration
===============================

## Geometry Types

Let's explore the types of geometries in the `natural_earth.countries` table.

```SQL
SELECT "ADMIN", st_numgeometries(geo::geometry), st_geometrytype(geo::geometry) from natural_earth.countries;
```
The table has both polygons and multipolygons. By default, a geometry/geography column can hold any type.

Lets insert a point feature:

```SQL
INSERT INTO natural_earth.countries ("ADMIN", geo) VALUES ('This is a point', st_makepoint(0,0));
```

One may, however, wish to constrain the types of geometries any given column can hold. This is accomplished with Postgresql's typedef feature. It is simlar to when you limit a varchar (ex: `varchar(4)`) or give a numeric type a certain precision. You can also constrain the SRID.
Let us modify the `natural_earth.countries` table to only allow storing multipolygons.

## Geometry/Geography Column Constraints

```SQL
ALTER TABLE natural_earth.countries ALTER COLUMN geo TYPE geography(Multipolygon, 4326);
```

We should get an error stating that Geometry type (Polygon) does not match column type (MultiPolygon). That is because, while it may seem intuitive that a polygon is just a form of multipolygon, that is not exactly the case. We need to force the polygons to multipolygons using the function `st_multi`.

```SQL
ALTER TABLE natural_earth.countries ALTER COLUMN geo TYPE geography(Multipolygon, 4326) USING st_multi(geo);
```

Another error: the annoying fact that most functions only work with geometries comes back to bite us. We should really petition the PostGIS developers to just implement a geography variant of all the funcions.

Also, we added an errant point earlier. So lets get rid of that as well.

```SQL
DELETE FROM natural_earth.countries WHERE st_geometrytype(geo::geometry) = 'ST_Point';
ALTER TABLE natural_earth.countries ALTER COLUMN geo TYPE geography(Multipolygon, 4326) USING st_multi(geo::geometry);
```
If we try and insert a point again, it should fail:

```SQL
INSERT INTO natural_earth.countries ("ADMIN", geo) VALUES ('This should not work', st_makepoint(0,0));
```

This does not prevent you from entering null values though.

```SQL
INSERT INTO natural_earth.countries ("ADMIN", geo) VALUES ('But this will', null);
```
So you still need a regular `NOT NULL` constraint on the column if you wish to avoid nulls.

```SQL
DELETE FROM natural_earth.countries WHERE geo IS NULL;
ALTER TABLE natural_earth.countries ALTER COLUMN geo SET NOT NULL;
```
## Here is a subtle Gotcha

```SQL
INSERT INTO natural_earth.countries ("ADMIN", geo)
  VALUES ('Betcha did''t think of this', 'MULTIPOLYGON(empty)'::geometry);
```

An empty multipolygon is still a multipolygon. But how can you get an empty geometry? Some simplification functions can make things *very* simple and return empty geometries.

If you have an idea of what your data is supposed to look like, you can use regular check constraints to enforce data integrity. For example, you may have a table of linestrings representing pipes, but you are sure you don't have any 5cm long pipes so you add a check constraint to make sure every linestring has a length of at least 10m.

Lets add a constraint to prevent empty geometries.

```SQL
ALTER TABLE natural_earth.countries ADD CONSTRAINT no_empties CHECK (NOT st_isempty(geo::geometry)) NOT VALID;
```

The `NOT VALID` construct will prevent new data from being added or updated that would fail the check. This way you can take time to fix the existing invalid data. Lets do so now:

```SQL
DELETE FROM natural_earth.countries WHERE st_isempty(geo::geometry);
ALTER TABLE natural_earth.countries VALIDATE CONSTRAINT no_empties;
```

## There is no escaping the inevitablity of invalidity

Sometimes, some geometries can be invalid; invalid geometries may give inaccurate answers.

```SQL
SELECT "ADMIN" country, st_isvalidreason(geo::geometry), st_isvalid(geo::geometry), st_issimple(geo::geometry)
FROM natural_earth.countries
WHERE NOT st_isvalid(geo::geometry) OR NOT st_issimple(geo::geometry);
```

Lets fix this geometry using st_makevalid.

```SQL
UPDATE natural_earth.countries
SET geo = st_makevalid(geo::geometry)
WHERE NOT st_isvalid(geo::geometry) OR NOT st_issimple(geo::geometry);
```

And lets add a constraint:

```SQL
ALTER TABLE natural_earth.countries ADD CONSTRAINT "Only valid & simple geo's please" CHECK
  (st_isvalid(geo::geometry) AND st_issimple(geo::geometry) );
```

## Takeaways

Your data is important and Postgresql has various mechanisms that you can use to ensure that the data you are storing makes sense for your use case. It is best to use constraints to enforce these requirements.