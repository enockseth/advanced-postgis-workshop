Advanced Postgis Workshop - foss4g-2022-firenze
===============================================

Requirements
------------

 - Git.
 - Containers. Docker/Podman. If able to orchestrate a docker-compose.yml file then you are on the right track.
 - A network connection.

Getting Started
---------------

Using docker-compose or similar:

`docker-compose up -d`

This will spin up all the relevant containers.

Sanity Checks
-------------

You should be able to exec into a container. For example:

```
podman exec advanced-postgis-workshop_pgsql_1 ls
```
OR
```
docker exec advanced-postgis-workshop_pgsql_1 ls
```
should give you a directory listing in the container. You should see: `🐘`.

More Sanity Checks
------------------

If you have `psql` on your machine then typing `./dbin.sh` should pop you into the database.
You can also access an instance of pgadmin4 at [http://localhost:44448](http://localhost:44448).

You can connect to the database using the client of your choice with the following:
 - host: localhost
	- port: 44449
	- dbname: workshop
	- user: workshop

Once you are connected execute

```SQL
CALL advanced_postgis_workshop();
```

This will download some data that will be used.

If that works for you then jump into Exercise #1 in the exercises folder.
